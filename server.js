var player = require('./player');
var express = require('express');
var bodyParser = require('body-parser');
var Player = require('./game/Player.js');
console.log(Player);
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(req, res){
  res.send(200, 'OK')
});

app.post('/', function(req, res) {
  console.log(req.body.action);
  if(req.body.action == 'bet_request') {
    res.send(200, Player.betRequest(req.body.game_state));
  } else if(req.body.action == 'showdown') {
    player.showdown(JSON.parse(req.body.game_state));
    res.send(200, 'OK');
  } else if(req.body.action == 'version') {
    res.send(200, Player.getVersion());
  } else {
    res.send(200, 'OK')
  }
});

const port = parseInt(process.env['PORT'] || 1337);
const host = "0.0.0.0";
app.listen(port, host);
console.log('Listening at http://' + host + ':' + port)

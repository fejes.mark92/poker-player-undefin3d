module.exports = {
    "extends": "airbnb-base",
    "env": {
      "es6": true
    },
    "installedESLint": true,
    "rules": {
      "comma-dangle": 0,
      "padded-blocks": 0,
      "linebreak-style": 0,
      "import/no-extraneous-dependencies": 0,
      "no-plusplus": 0
    },
    "plugins": [
        "import"
    ]
};

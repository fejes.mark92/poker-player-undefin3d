const ClassChenF0rgMula = require('../../functions/ChenF0rgMula/ClassChenF0rgMula.js');
const ClassCard = require('../../functions/ChenF0rgMula/ClassCard.js');

class evalChen {
  applyRule(gameState) {
    const cards = gameState.players[gameState.in_action].hole_cards;
    const card1 = new ClassCard(cards[0].rank, cards[0].suit);
    const card2 = new ClassCard(cards[1].rank, cards[1].suit);
    const formula = new ClassChenF0rgMula(card1, card2);
    const result = formula.getValue();

    let hold = 0;
    let raise = 0;
    let call = 0;

    let activePlayers = 0;
    for (let i = 0; i < gameState.players.length; i++) {
      if (gameState.players[i].status !== 'out') {
        activePlayers += activePlayers + 1;
      }
    }
    if (activePlayers > 4) { // 5-6 player
      console.log('5-6 player');
      if (result > 16) {
        raise += 1;
      } else {
        hold += 1;
      }
    } else if (activePlayers > 2) { // 3-4 player
      console.log('3-4 player');
      if (result >= 15) {
        raise += 1;
      } else {
        hold += 1;
      }
    } else if (activePlayers > 0) { // 2player
      console.log('2 player');
      if (result >= 14) {
        raise += 1;
      } else {
        hold += 1;
      }
    }

    call = 0;
    return [hold, call, raise];
    // todo gameState
  }
}

module.exports = evalChen;

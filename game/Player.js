const ClassChenF0rgMula = require('./functions/ChenF0rgMula/ClassChenF0rgMula.js');
const ClassCard = require('./functions/ChenF0rgMula/ClassCard.js');

const PLAYER_VERSION = 'Error: undefined is undfined';
const RuleManager = require('./RuleManager');

const evalChen = require('./rules/preflop/evalChen.js');


class Player {
  static init() {

  }

  static getVersion() {
    return PLAYER_VERSION;
  }

  static checkPlayer() {
    return 'Undefin3d her e, yo!';
  }

  static showdown(gameState) {
    // TODO store gameState
    return 'Ok';
  }

  static betRequest(gameState) {
    try {
      console.log('betRequest()');
      const parsedGameState = JSON.parse(gameState);
      const ruleManager = new RuleManager(parsedGameState);
      if (parsedGameState.community_cards.length === 0) {
        // preflop
        ruleManager.addRule(new evalChen());
      } else {
        // flop
        ruleManager.addRule(new evalChen());
      }

      console.log('applyRules()');
      ruleManager.applyRules();
      const decision = ruleManager.makeDecision();
      console.log('decision');
      console.log(decision);
      if (decision === 'fold') {
        return 0;
      } else if (decision === 'call') {
        return parsedGameState.current_buy_in;
      } else if (decision === 'raise') {
        if (parsedGameState.community_cards.length === 0) {
          // preflop
          if (parsedGameState.minimum_raise > parsedGameState.small_blind * 2 * 3) {
            return parsedGameState.players[parsedGameState.in_action].stack;
          }
          return parsedGameState.small_blind * 2 * 3;
        } else {
          // flop
          return parsedGameState.players[parsedGameState.in_action].stack;
        }
      }
      return 0;
    } catch (e) {
      console.log(e);
      console.log(e.stack);
      return 0;
    }
  }

  static evaluatePreFlop(number) {
    let hold = 0;
    let raise = 0;
    let call = 0;

    if (number >= 14) {
      raise += 1;
    } else {
      hold += 1;
    }
    call = 0;
    return { hold, call, raise };
/*
if the calculated value is greater than 31
  check_fold = check_fold + 0
  raise = raise + 80
  call = call + 20

if the calculated value is 28
  check_fold = check_fold + 0
  raise = raise + 70
  call = call + 30


if the calculated value is 24
  check_fold = check_fold + 0
  raise = raise + 60
  call = call + 40

if the calculated value is 20
  check_fold = check_fold + 40
  raise = raise + 20
  call = call + 20

if the calculated value is grater than 14 and lesser than 20
  check_fold = check_fold + 70
  raise = raise + 30
  call = call + 0
*/
  }

}

module.exports = Player;

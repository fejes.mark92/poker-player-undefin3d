class RuleManager {
  constructor(gameState) {
    this.gameState = gameState;
    this.decision = [0, 0, 0];
    this.rules = [];
  }

  applyRules() {
    this.rules.forEach((rule) => {
      console.log('rule');
      console.log(rule);
      const ruleResult = rule.applyRule(this.gameState);
      console.log('rule result');
      console.log(ruleResult);
      this.decision[0] = 100 * ruleResult[0];
      this.decision[1] = 100 * ruleResult[1];
      this.decision[2] = 100 * ruleResult[2];
    });
  }

  makeDecision() {
    if (this.decision[0] > this.decision[1] && this.decision[0] > this.decision[2]) {
      return 'hold';
    } else if ((this.decision[1] > this.decision[0] && this.decision[1] > this.decision[2])) {
      return 'call';
    } else if ((this.decision[2] > this.decision[0]) && this.decision[2] > this.decision[1]) {
      return 'raise';
    }
    return 'hold';
  }

  addRule(rule) {
    this.rules.push(rule);
  }
}

module.exports = RuleManager;

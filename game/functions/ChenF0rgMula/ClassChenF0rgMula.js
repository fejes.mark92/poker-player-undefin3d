/* eslint-disable */

function setValues (aCard1, aCard2) {
  switch (aCard1.value) {
    case '1': aCard1.f0rgValue = 1; aCard1.chenValue = 1; break;
    case '2': aCard1.f0rgValue = 2; aCard1.chenValue = 2; break;
    case '3': aCard1.f0rgValue = 3; aCard1.chenValue = 3; break;
    case '4': aCard1.f0rgValue = 4; aCard1.chenValue = 4; break;
    case '5': aCard1.f0rgValue = 5; aCard1.chenValue = 5; break;
    case '6': aCard1.f0rgValue = 6; aCard1.chenValue = 6; break;
    case '7': aCard1.f0rgValue = 7; aCard1.chenValue = 7; break;
    case '8': aCard1.f0rgValue = 8; aCard1.chenValue = 8; break;
    case '9': aCard1.f0rgValue = 9; aCard1.chenValue = 9; break;
    case '10': aCard1.f0rgValue = 10; aCard1.chenValue = 10; break;

    case 'J': aCard1.f0rgValue = 11; aCard1.chenValue = 12; break;
    case 'Q': aCard1.f0rgValue = 12; aCard1.chenValue = 14; break;
    case 'K': aCard1.f0rgValue = 13; aCard1.chenValue = 16; break;
    case 'A': aCard1.f0rgValue = 14; aCard1.chenValue = 20; break;
  }
  switch (aCard2.value) {
    case '1': aCard2.f0rgValue = 1; aCard2.chenValue = 1; break;
    case '2': aCard2.f0rgValue = 2; aCard2.chenValue = 2; break;
    case '3': aCard2.f0rgValue = 3; aCard2.chenValue = 3; break;
    case '4': aCard2.f0rgValue = 4; aCard2.chenValue = 4; break;
    case '5': aCard2.f0rgValue = 5; aCard2.chenValue = 5; break;
    case '6': aCard2.f0rgValue = 6; aCard2.chenValue = 6; break;
    case '7': aCard2.f0rgValue = 7; aCard2.chenValue = 7; break;
    case '8': aCard2.f0rgValue = 8; aCard2.chenValue = 8; break;
    case '9': aCard2.f0rgValue = 9; aCard2.chenValue = 9; break;
    case '10': aCard2.f0rgValue = 10; aCard2.chenValue = 10; break;

    case 'J': aCard2.f0rgValue = 11; aCard2.chenValue = 12; break;
    case 'Q': aCard2.f0rgValue = 12; aCard2.chenValue = 14; break;
    case 'K': aCard2.f0rgValue = 13; aCard2.chenValue = 16; break;
    case 'A': aCard2.f0rgValue = 14; aCard2.chenValue = 20; break;
  }
}

function checkIfPair (aCard1, aCard2) {
  if (aCard1.value === aCard2.value) {
    return true;
  } else {
    return false;
  }
}

function checkIfSuited (aCard1, aCard2) {
  if (aCard1.suit === aCard2.suit) {
    return true;
  } else {
    return false;
  }
}

function gapCalculation (aCard1, aCard2) {
  var gap = 0;
  if (aCard1.f0rgValue > aCard2.f0rgValue) {
    gap = aCard1.f0rgValue - aCard2.f0rgValue - 1;

    //console.log('gap: ' + gap);
  } else {
    gap = aCard2.f0rgValue - aCard1.f0rgValue - 1;
    //console.log('gap: ' + gap);
  }

  var value = -10;
  switch (gap) {
    case 0: value = 0; break;
    case 1: value = -2; break;
    case 2: value = -4; break;
    case 3: value = -8; break;
  }

  //////////////FINAL CORRECTION/////////////
  if (gap <= 1 && aCard1.f0rgValue < 12 && aCard2.f0rgValue < 12) {
    value += 2;
  }
  return value;
}

class ClassChenF0rgMula {
  constructor (aCard1, aCard2) {
    console.log('Card1: ' + aCard1.value + ' ' + aCard1.suit);
    console.log('Card2: ' + aCard2.value + ' ' + aCard2.suit);

    //console.log('1: ' + aCard1.f0rgValue);
    setValues(aCard1, aCard2);
    //console.log('2: ' + aCard1.f0rgValue);
    this.higherValue = 0;
    if(aCard1.chenValue > aCard2.chenValue) {
      this.higherValue = aCard1.chenValue;
    } else {
      this.higherValue = aCard2.chenValue;
    }

    if (checkIfPair(aCard1, aCard2)) {
      this.endValue = this.higherValue * 2;
      console.log('EndValue: ' + this.endValue);
    } else {
        this.endValue = this.higherValue;
      if (checkIfSuited(aCard1, aCard2)) {
        this.endValue = this.higherValue + 4;
      }
      this.endValue += gapCalculation(aCard1, aCard2);
      console.log('EndValue: ' + this.endValue);
    }

    /*if (checkIfSuited(aCard1, aCard2)) {
      this.endValue = this.higherValue * 2;
      console.log('Suited! Final value: ' + this.endValue);
    } else {
      console.log('not suited..');
      console.log('higher value: ' + this.higherValue);
      this.endValue = this.higherValue +   4;
      //console.log(gapCalculation(aCard1, aCard2));
      this.temp = gapCalculation(aCard1, aCard2);
      console.log(this.temp + ' asd ' + this.endValue);
      this.endValue += this.temp;
      console.log('Final Value: ' + this.endValue);
    }*/
  }

   getValue() {
    return this.endValue;
  }
}

module.exports = ClassChenF0rgMula;
